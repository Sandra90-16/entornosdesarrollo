package estructurasdecontrol;

/*este programa calcula el m�ximo com�n 
divisor de 2 n�meros*/

import java.util.Scanner;

public class Mcd {

	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in);
		//introducimos por teclado los n�meros.
		System.out.println("introduzca el n�mero 1");
		int num1 = entrada.nextInt();
		System.out.println("introduzca el n�mero 2");
		int num2 = entrada.nextInt();
		//comparamos el num 1 con el num2.
		while(num1 != num2)
		{
			if(num1 > num2)
				num1 = num1 - num2;
			else
				num2 = num2 - num1;
		
		}
		System.out.println("el m�ximo comun divisor = " + num1);
	}
}