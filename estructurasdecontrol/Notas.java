package estructurasdecontrol;

import java.util.Scanner;

public class Notas {

	public static void main(String[] args) {
	
		/*Este programa visualiza en pantalla la
		 * nota de 25 alumnos, siendo la m�s alta un 10
		 * y la m�s baja un 1.
		 */
		
		Scanner entrada = new Scanner(System.in);
		double notaIntroducida = 0;
		int numNotasIntroducidas = 25;
		double notaMax = 0;
		double notaMin = 10;
		while (numNotasIntroducidas > 0) {
			System.out.println("Introduzca una nota: ");
			notaIntroducida = entrada.nextInt();
			if (notaIntroducida >10 || notaIntroducida <1)  {  
				System.out.println("ERROR, la nota debe estar entre 1 y 10");	
				
			} else {
				numNotasIntroducidas--;
				System.out.println("La nota introducida es: " + notaIntroducida  );
				if(numNotasIntroducidas == 25) {
					notaMax = notaIntroducida;
					notaMin = notaIntroducida;
				}
				if (notaIntroducida > notaMax) {
					notaMax = notaIntroducida;
				}
				if (notaIntroducida < notaMin) {
					notaMin = notaIntroducida;
				}		
			}
		}
		System.out.println("La nota m�s alta es: " + notaMax + " y la nota m�s baja es: "
				+ notaMin);
		
	}
}
