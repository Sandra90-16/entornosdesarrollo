package estructurasdecontrol;

import java.io.IOException;

public class DoWhile2 {

	public static void main(String[] args) throws IOException {
		
		/*Introduce por teclado una respuesta v�lida.
		 * La respuesta es v�lida cuando sea una 's' may�scula
		 * o una 'n' min�scula
		 */
		
		boolean respuestaValida = true;
		char respuesta;
		System.out.println("Introduzca la respuesta");
		do {
			respuesta = (char)System.in.read();
			//Limpiamos el buffer de teclado
			System.in.skip(2);
			respuesta = Character.toLowerCase(respuesta);
			//comprobamos si la respuesta es valida
			respuestaValida = (respuesta == 's' || respuesta == 'n');
			
			if(!respuestaValida)
				System.out.println("error: teclea una 's' o una 'n'");
			
		}while (!respuestaValida);
		
		System.out.println("fin del programa");
		
		/*cuando metemos un caracter por teclado, este se guarda con 
		\n*/
		
		//

	}

}
