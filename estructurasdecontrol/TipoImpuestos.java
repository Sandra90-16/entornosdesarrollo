package estructurasdecontrol;

import java.util.Scanner;

public class TipoImpuestos {

	public static void main(String[] args) {
	/*Este programa pregunta al usuario su renta anual 
	 *y muestra por pantalla el tipo impositivo que le corresponde*/
	
		
		Scanner entrada = new Scanner(System.in);
		
		//Introducimos por teclado el tipo de renta
		
		 
		System.out.println("Introduzca su tipo de renta");
		int rentaAnual = entrada.nextInt();
		int porcentajeImpuestos;
		if(rentaAnual <= 0)
			System.out.println("la renta tiene que ser mayor que 0");
			 
		else {
			if(rentaAnual<10000) {
				porcentajeImpuestos=5;
			}
			else if(rentaAnual<20000) {
				porcentajeImpuestos = 15;
			}
			else if(rentaAnual <35000) {
				porcentajeImpuestos = 20;
			}
			else if(rentaAnual <60000) {
				porcentajeImpuestos = 30;
			}
			else {
				porcentajeImpuestos = 45; 
			}
			System.out.println("el tipo impositivo es " + porcentajeImpuestos + "%");
		 }
	}

}