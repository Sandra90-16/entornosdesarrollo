package CabreraSandraProgramacion;

import java.util.Scanner;

public class EsPrimo {

	public static void main(String[] args) {
	   
		System.out.println("Introduzca un n�mero: ");
		Scanner entrada = new Scanner(System.in);
		int num = entrada.nextInt();
        int cont = 0;
        esPrimo(num, cont);
        
        } 
	

	private static void esPrimo(int num, int cont) {
		
		for(int i = 2; i < num; i++){                
            if(num % i == 0){
                System.out.print("NO ES PRIMO\n");
                cont = 1;
                break;
            }
        }
        
        if(cont == 0){
    	  System.out.print("ES PRIMO\n");
        }	
	}
}
