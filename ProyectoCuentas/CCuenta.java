package ProyectoCuentas;

public abstract class CCuenta{
		
		//Atributos
		
		private String numCuenta;
		private String nombreTitular;
		protected double saldo;
		protected double tipoDeInteres;
		
		//Constructores con parámetros
		public CCuenta(String numCuenta, String nombre, double saldo, double tipoDeInteres, double comisiones) {
			this.numCuenta = numCuenta;
			this.nombreTitular = nombre;
			this.saldo = saldo;
			this.tipoDeInteres = tipoDeInteres;
		}
		//constructor por defecto, sin parámetros
		public CCuenta() {
			
		}

		public String getNumCuenta() {
			return numCuenta;
		}

		public void setNumCuenta(String numCuenta) {
			this.numCuenta = numCuenta;
		}

		public String getNombre() {
			return nombreTitular;
		}

		public void setNombre(String nombre) {
			this.nombreTitular = nombre;
		}

		public double getSaldo() {
			return saldo;
		}

		public void setSaldo(double saldo) {
			this.saldo = saldo;
		}
		public String getNombreTitular() {
			return nombreTitular;
		}
		public void setNombreTitular(String nombreTitular) {
			this.nombreTitular = nombreTitular;
		}
		public double getTipoDeInteres() {
			return tipoDeInteres;
		}
		public void setTipoDeInteres(double tipoDeInteres) {
			this.tipoDeInteres = tipoDeInteres;
		}

		public void ingreso(double cantidad){
			saldo = saldo + cantidad;
		}
		
		public void reintegro(double cantidad) {
			saldo = saldo - cantidad;
		}
		
		//misma estructura
		public abstract double interes();
		
		public abstract void comisiones();
	}

