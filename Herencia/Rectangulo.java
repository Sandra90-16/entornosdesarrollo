package Herencia;

public class Rectangulo extends FiguraPlana {

	public Rectangulo(double base, double altura) {
		super(base, altura);
		
	}

	@Override
	public double area() {
		
		return base * altura;
	}

}
