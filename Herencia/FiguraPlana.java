package Herencia;
/**
 * figura plana es una clase abstracta. Una clase abstracta es aquella que tiene por lo menos un m�todo abstracto. 
 * un m�todo abstracto es una herramienta que tiene java para implementar polimorfismo.
 * No se pueden emplear objetos en una clase abstracta.
 * @author Sandra
 *
 */
public abstract class FiguraPlana {
 
	//Atributos base y altura
	
	protected double base;
	protected double altura;
	public FiguraPlana(double base, double altura) {
		this.base = base;
		this.altura = altura;
	}
	public double getBase() {
		return base;
	}
	public void setBase(double base) {
		this.base = base;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	public String mostrarDatos() {
		return "\n" + base + "\n" + altura; 
	}
	////////////////////////////////
	//m�todo abstracto
	public abstract double area();
	
	
}
