package POO;

import java.util.Scanner;

public class Cadena {

	public static void main(String[] args){
		//declaramos un objeto
		Scanner entrada = new Scanner(System.in);
		String str1= new String("Hola world!");
		System.out.println("la cadena es " + str1);
		int longitud = str1.length();
		System.out.println("la longitud de la cadena es " + longitud);
		//Almacena en una variable el car�cter que ocupa la cuarta posici�n de la cadena y visualiza su contenido.
		char ch = str1.charAt(4);
		System.out.println("la cuarta posici�n es " + ch);
		// Construye tres objetos S1,S2 y S3. Inicia S1 y S2 con la cadena �Hola Mundo� y S3 con la cadena �Hola mundo�. 
	
		String S1= new String("Hola Mundo");
		String S2= new String("Hola Mundo");
		String S3= new String("Hola mundo");
		 //Compara S1 con S2 indicando si son iguales. Haz lo mismo con S1 y S3 ignorando la diferencia entre may�sculas y min�sculas.
		//compareTo (compara 2 cadenas)
		if(S1.equals(S2)){
			System.out.println("S1 y S2 son iguales");
		}
		
			if(S1.equalsIgnoreCase(S3)){
			System.out.println("S1 y S3 son iguales");
		}else {
			System.out.println("S1 y S3 no son iguales");
		}
		// Compara si las cadena S2 y S3 l�xicamente iguales.
		if(S2.equals(S3)){
			System.out.println("S2 y S3 son iguales");
		}else {
			System.out.println("S2 y S3 no son iguales");
			
			}
		// Visualiza la primera posici�n o �ndice del car�cter �u� dentro de la cadena
		S1= "Hola Mundo";
		System.out.println("la posici�n de 'u' es " + (S1.indexOf('u')));
		
			//Sustituye el car�cter �o� por �e� en S2.
		S2 = "Hola Mundo";
		System.out.println(S2.replace("o", "e"));
		/*Declara una variable Num de tipo String,
		 * inicia un double con 5.6, transforma el double en una cadena almacenando su contenido en Num. Visualiza Num. 
		 */
		String num;
		double num1 = 5.6;
		num = Double.toString(num1);
		//String.valueOf(num)
		System.out.println("el n�mero es " + num);
		
				
		//Pide por teclado una cadena. Visualiza su contenido. 
		//A continuaci�n visualiza el mensaje �Cadena invertida� junto con la cadena invertida utiliza para ello los m�todos aprendidos.
     
		System.out.println("Introduzca una frase: ");
		String frase= "";
		frase = entrada.nextLine();
		System.out.println("su frase es " + frase);
		for(int i =frase.length();i>0;i--){
			 
			System.out.println("la  cadena invertida es " + (frase.charAt(i-1)));
		//for(int i = frase.length(); i>0;i--){
		//syso (frase.charAt(i-1));
		}
	} 
}
