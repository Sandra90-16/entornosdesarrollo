package POO.Circulo;


public class Circulo {

		
		//Atributos
		private double radio;
		
		//Constructor con radio como argumento
		public Circulo(double radio) {
			this.radio = radio;
		}
		
		//este constructor asigna valor 2 al radio.
		//Los constructores siempre se llaman igual que la clase.
		public Circulo() {
			this.radio=2;
		}
		
		//Cuando un m�todo devuelve algo, en la declaracion se pone el tipo
		//en este m�todo es 'double'
		public double calcularSuperficie() {
			return 3.1416 * radio;
		}
		
}