package PracticaExamen;

import java.util.Scanner;

public class EsVocal {

	public static void main(String[] args) {
		
		
		System.out.println("Introduzca una letra: ");
		Scanner entrada = new Scanner(System.in);
		char letraIntroducida = entrada.next().charAt(0);

		esVocal(letraIntroducida);
	
	
	}
	
	public static void esVocal(char letraIntroducida) {
		letraIntroducida = Character.toLowerCase(letraIntroducida);
		switch(letraIntroducida) {
	case 'a':
	   case 'e':
	   case 'i':
	   case 'o':
	   case 'u':
	     System.out.println("es vocal");
	     break;
	   default:
		 System.out.println("es consonante");
		 break;
		// con el break se acaba el switch.
		}
	}
}